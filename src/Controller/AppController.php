<?php

namespace App\Controller;

use App\Entity\Stl;
use App\Entity\Faction;
use App\Entity\Race;
use App\Entity\Unite;
use App\Entity\Contact;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class AppController extends AbstractController
{

    public function actualite()
    {
        $aos = $this->getDoctrine()->getRepository(Unite::class)->findBy(['age' => 1], ['id' => 'desc'], 2);
        $stl_aos = $this->getDoctrine()->getRepository(Stl::class)->findBy(['age' => 1], ['id' => 'desc'], 2);
        $war40k = $this->getDoctrine()->getRepository(Unite::class)->findBy(['age' => 2], ['id' => 'desc'], 2);
        $stl_war40k = $this->getDoctrine()->getRepository(Stl::class)->findBy(['age' => 2], ['id' => 'desc'], 2);
        return $this->render('app/actualite.html.twig', [
            'unite' => $aos, 'war' => $war40k, 'stl_aos' => $stl_aos, 'stl_war40k' => $stl_war40k
        ]);
    }

    public function warhammer40k()
    {
        $em = $this->getDoctrine()->getManager();
        $faction = $em->getRepository(Faction::class)->findBy(['age' => 2]);
        return $this->render('app/warhammer40k.html.twig', [
            'faction' => $faction
        ]);
    }

    public function warhammer40k_details($id)
    {
        $em = $this->getDoctrine()->getManager();
        $race = $em->getRepository(Race::class)->findBy(['faction' => $id]);
        $faction = $em->getRepository(Faction::class)->findOneBy(['id' => $id]);
        return $this->render('app/warhammer40k_details.html.twig', [
            'race' => $race, 'faction' => $faction
        ]);
    }

    public function lore()
    {

        return $this->render('app/lore.html.twig');
    }


    public function warhammeraos()
    {

        $em = $this->getDoctrine()->getManager();
        $faction = $em->getRepository(Faction::class)->findBy(['age' => 1]);
        return $this->render('app/warhammeraos.html.twig', [
            'faction' => $faction
        ]);
    }

    public function warhammeraos_details($id)
    {
        $em = $this->getDoctrine()->getManager();
        $race = $em->getRepository(Race::class)->findBy(['faction' => $id]);
        $faction = $em->getRepository(Faction::class)->findOneBy(['id' => $id]);
        return $this->render('app/warhammeraos_details.html.twig', [
            'race' => $race, 'faction' => $faction
        ]);
    }

    public function race($id)
    {
        $em = $this->getDoctrine()->getManager();
        $stl = $em->getRepository(Stl::class)->findBy(['race' => $id]);
        $unite = $em->getRepository(Unite::class)->findBy(['race' => $id]);
        $race = $em->getRepository(Race::class)->findOneBy(['id' => $id]);
        return $this->render('app/race.html.twig', [
            'race' => $race, 'unite' => $unite, 'stl' => $stl
        ]);
    }

    public function debuter_warhammer()
    {
        return $this->render('app/debuter_warhammer.html.twig');
    }

    public function impression3d()
    {
        return $this->render('app/impression3d.html.twig');
    }
    public function contact(MailerInterface $mailer, Request $request)
    {

        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {


            $emaildata = $form->getData();

            $email = (new Email())
                ->from($emaildata->getEmail())
                ->to('Contact@Warhammerfornoob.com')
                ->subject($emaildata->getName())
                ->html($emaildata->getMessage());

            $mailer->send($email);
            $this->addFlash('success', 'Message envoyé!');

            return $this->redirect($request->getUri());
        }

        return $this->render('app/contact.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
