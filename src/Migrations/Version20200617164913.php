<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200617164913 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, roles VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE faction CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE race CHANGE faction_id faction_id INT DEFAULT NULL, CHANGE unite_id unite_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stl CHANGE race_id race_id INT DEFAULT NULL, CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE unite CHANGE age_id age_id INT DEFAULT NULL, CHANGE race_id race_id INT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE user');
        $this->addSql('ALTER TABLE faction CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE race CHANGE faction_id faction_id INT DEFAULT NULL, CHANGE unite_id unite_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stl CHANGE race_id race_id INT DEFAULT NULL, CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE unite CHANGE age_id age_id INT DEFAULT NULL, CHANGE race_id race_id INT DEFAULT NULL');
    }
}
