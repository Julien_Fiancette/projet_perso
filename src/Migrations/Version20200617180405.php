<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200617180405 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE faction CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE race CHANGE faction_id faction_id INT DEFAULT NULL, CHANGE unite_id unite_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stl CHANGE race_id race_id INT DEFAULT NULL, CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE unite CHANGE age_id age_id INT DEFAULT NULL, CHANGE race_id race_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE faction CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE race CHANGE faction_id faction_id INT DEFAULT NULL, CHANGE unite_id unite_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE stl CHANGE race_id race_id INT DEFAULT NULL, CHANGE age_id age_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE unite CHANGE age_id age_id INT DEFAULT NULL, CHANGE race_id race_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
