<?php

namespace App\Form;

use App\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;




class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => "Nom et Prénom",
                'required' => true,
            ])
            ->add("email", EmailType::class, [
                'label' => "Votre adresse mail",
                'required' => true,
            ])
            ->add("message", TextareaType::class, [
                'label' => "Votre message",
                'required' => true,
                'constraints' => [
                    new Assert\Length([
                        'min' => 10,
                        'minMessage' => "Le message doit comporter au moins 10 caractères.",
                        'max' => 500,
                        'maxMessage' => "Le message doit faire moins de 500 caractères.",
                    ]),
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Contact::class
        ]);
        
    }
}