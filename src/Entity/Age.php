<?php

namespace App\Entity;

use App\Repository\AgeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AgeRepository::class)
 */
class Age
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity=Unite::class, mappedBy="age")
     */
    private $version;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Faction::class, mappedBy="age")
     */
    private $faction;

    /**
     * @ORM\OneToMany(targetEntity=Stl::class, mappedBy="age")
     */
    private $stls;

    public function __construct()
    {
        $this->version = new ArrayCollection();
        $this->faction = new ArrayCollection();
        $this->stls = new ArrayCollection();
    }

    

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Unite[]
     */
    public function getVersion(): Collection
    {
        return $this->version;
    }

    public function addVersion(Unite $version): self
    {
        if (!$this->version->contains($version)) {
            $this->version[] = $version;
            $version->setAge($this);
        }

        return $this;
    }

    public function removeVersion(Unite $version): self
    {
        if ($this->version->contains($version)) {
            $this->version->removeElement($version);
            // set the owning side to null (unless already changed)
            if ($version->getAge() === $this) {
                $version->setAge(null);
            }
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function __toString()
    {

        return $this->name;
    }

    /**
     * @return Collection|Faction[]
     */
    public function getFaction(): Collection
    {
        return $this->faction;
    }

    public function addFaction(Faction $faction): self
    {
        if (!$this->faction->contains($faction)) {
            $this->faction[] = $faction;
            $faction->setAge($this);
        }

        return $this;
    }

    public function removeFaction(Faction $faction): self
    {
        if ($this->faction->contains($faction)) {
            $this->faction->removeElement($faction);
            // set the owning side to null (unless already changed)
            if ($faction->getAge() === $this) {
                $faction->setAge(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Stl[]
     */
    public function getStls(): Collection
    {
        return $this->stls;
    }

    public function addStl(Stl $stl): self
    {
        if (!$this->stls->contains($stl)) {
            $this->stls[] = $stl;
            $stl->setAge($this);
        }

        return $this;
    }

    public function removeStl(Stl $stl): self
    {
        if ($this->stls->contains($stl)) {
            $this->stls->removeElement($stl);
            // set the owning side to null (unless already changed)
            if ($stl->getAge() === $this) {
                $stl->setAge(null);
            }
        }

        return $this;
    }


}
