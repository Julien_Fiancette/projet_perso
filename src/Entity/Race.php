<?php

namespace App\Entity;

use App\Repository\RaceRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RaceRepository::class)
 */
class Race
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Name;

    /**
     * @ORM\ManyToOne(targetEntity=Faction::class, inversedBy="race")
     */
    private $faction;

    /**
     * @ORM\ManyToOne(targetEntity=Unite::class, inversedBy="race")
     */
    private $unite;

    /**
     * @ORM\Column(type="text")
     */
    private $lore;

    /**
     * @ORM\OneToMany(targetEntity=Stl::class, mappedBy="race")
     */
    private $stls;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $img;

    public function __construct()
    {
        $this->unites = new ArrayCollection();
        $this->stls = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->Name;
    }

    public function setName(string $Name): self
    {
        $this->Name = $Name;

        return $this;
    }

    public function getFaction(): ?Faction
    {
        return $this->faction;
    }

    public function setFaction(?Faction $faction): self
    {
        $this->faction = $faction;

        return $this;
    }

    public function getUnite(): ?Unite
    {
        return $this->unite;
    }

    public function setUnite(?Unite $unite): self
    {
        $this->unite = $unite;

        return $this;
    }

    public function getLore(): ?string
    {
        return $this->lore;
    }

    public function setLore(string $lore): self
    {
        $this->lore = $lore;

        return $this;
    }
    
    /**
     * @return Collection|Stl[]
     */
    public function getStls(): Collection
    {
        return $this->stls;
    }

    public function addStl(Stl $stl): self
    {
        if (!$this->stls->contains($stl)) {
            $this->stls[] = $stl;
            $stl->setRace($this);
        }

        return $this;
    }

    public function removeStl(Stl $stl): self
    {
        if ($this->stls->contains($stl)) {
            $this->stls->removeElement($stl);
            // set the owning side to null (unless already changed)
            if ($stl->getRace() === $this) {
                $stl->setRace(null);
            }
        }

        return $this;
    }
    
    public function __toString()
    {

        return $this->Name;
    }

    public function getImg(): ?string
    {
        return $this->img;
    }

    public function setImg(?string $img): self
    {
        $this->img = $img;

        return $this;
    }

}
