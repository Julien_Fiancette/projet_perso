<?php

namespace App\Repository;

use App\Entity\Stl;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stl|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stl|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stl[]    findAll()
 * @method Stl[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StlRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stl::class);
    }

    // /**
    //  * @return Stl[] Returns an array of Stl objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Stl
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
